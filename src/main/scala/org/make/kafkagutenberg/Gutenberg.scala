package org.make.kafkagutenberg

import zio.{http, ZIO}

object Gutenberg {
  private val location = http.URL.Location.Absolute(http.Scheme.HTTPS, "gutenberg.org", http.Scheme.HTTPS.defaultPort)

  // https://gutenberg.org/cache/epub/feeds/pg_catalog.csv.gz
  private val catalogUrl = http.URL(
    http.Root / "cache/epub/feeds/pg_catalog.csv.gz",
    location
  )

  def catalog                 = http.Client.request(http.Request.get(catalogUrl))
  def bookContent(id: String) = followRedirects(http.Request.get(http.URL(http.Root / "ebooks" / s"${id}.txt.utf-8", location)))

  private def followRedirects(req: http.Request) = {
    def handleResponse(resp: http.Response): ZIO[http.Client, Throwable, http.Response] =
      (resp.status, resp.header(http.Header.Location)) match {
        case (status, Some(header)) if status.isRedirection =>
          http.Client.request(http.Request.get(header.url)).flatMap(handleResponse)

        case _ =>
          ZIO.succeed(resp)
      }

    http.Client.request(req).flatMap(handleResponse)
  }
}
