package org.make.kafkagutenberg

import org.apache.commons.csv.{CSVFormat, CSVParser, CSVRecord}

import zio.stream.{ZPipeline, ZStream}
import zio.{json, ExitCode, Scope, ZIO}

import java.nio.charset.StandardCharsets.UTF_8

object GutenbergProducer {
  import KafkaZIO._

  def run(topic: String) = ZIO.scoped {
    for {
      producer <- openProducer[String, String](
        "bootstrap.servers" -> "localhost:9092",
        "acks"              -> "all"
      )
      response <- Gutenberg.catalog
      bookCount <- response.body.asStream
        .via(ZPipeline.gunzip())
        .via(ZPipeline.decodeCharsWith(UTF_8))
        .via(decodeCSV(CSVFormat.DEFAULT))
        .drop(1)
        .map(Book.fromCSV)
        .runFoldZIO(0)((count, book) =>
          producer.offer(topic, book.id, json.JsonCodec[Book].encodeJson(book, None).toString()).as(count + 1)
        )
      _ <- zio.Console.printLine(s"Published $bookCount books")
    } yield ExitCode.success
  }

  private def decodeCSV(format: CSVFormat): ZPipeline[Scope, Throwable, Char, CSVRecord] =
    ZPipeline.fromFunction(stream =>
      ZStream.fromZIO(stream.toReader.map(reader => ZStream.fromJavaStream(CSVParser.parse(reader, format).stream()))).flatMap(x => x)
    )
}
