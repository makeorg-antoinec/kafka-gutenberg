package org.make.kafkagutenberg

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{Serializer, StringSerializer}

import zio.{Scope, UIO, URIO, ZIO}

import java.util.Properties

object KafkaZIO {
  implicit val kafkaStringSerializer: Class[StringSerializer] = classOf[StringSerializer]

  def openProducer[K, V](
      config: (String, String)*
  )(implicit kser: Class[_ <: Serializer[K]], vser: Class[_ <: Serializer[V]]): URIO[Scope, KafkaProducer[K, V]] = {
    def initProps = {
      val props = new Properties()
      props.put("key.serializer", kser)
      props.put("value.serializer", vser)
      props
    }
    val props = config.foldLeft(initProps) { (acc, entry) =>
      acc.setProperty(entry._1, entry._2)
      acc
    }
    ZIO.acquireRelease(ZIO.succeed(new KafkaProducer[K, V](props)))(prod => ZIO.attempt(prod.close()).ignoreLogged)
  }

  implicit class KafkaProducerExt[K, V](private val producer: KafkaProducer[K, V]) extends AnyVal {
    def offer(topic: String, key: K, value: V): UIO[Unit] =
      ZIO.fromFutureJava(producer.send(new ProducerRecord(topic, key, value))).orDie.unit
  }
}
