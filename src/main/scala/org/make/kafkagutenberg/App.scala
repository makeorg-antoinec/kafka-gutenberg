package org.make.kafkagutenberg

import zio.cli.{CliApp, Command, HelpDoc, Options, ZIOCliDefault}
import zio.{http, ZIO, ZLayer}

object App extends ZIOCliDefault {

  sealed trait Action
  final case class Produce(topic: Option[String])                      extends Action
  final case class Transform(from: Option[String], to: Option[String]) extends Action
  final case class Consume(topic: Option[String])                      extends Action

  private val produceCommand = Command("produce", Options.text("topic").optional).map(Produce(_))
  private val transformCommand =
    Command("transform", Options.text("from").optional ++ Options.text("to").optional).map(x => Transform(x._1, x._2))
  private val consumeCommand = Command("consume", Options.text("topic").optional).map(Consume(_))
  private val mainCommand    = Command("kafkagutenberg").subcommands(produceCommand, transformCommand, consumeCommand)

  private val bookTopic      = "gutenberg.books"
  private val wordCountTopic = "words.count"

  override def cliApp =
    CliApp.make("kafkagutenberg", "0.1.0-SNAPSHOT", HelpDoc.Span.text("Read and analyze Gutenberg book collection."), mainCommand) {
      case Produce(maybeTopic) =>
        GutenbergProducer
          .run(maybeTopic.getOrElse(bookTopic))
          .provide(
            http.Client.customized,
            http.netty.client.NettyClientDriver.live,
            http.DnsResolver.default,
            ZLayer.succeed(http.netty.NettyConfig.default),
            ZLayer.succeed(http.Client.Config.default)
          )

      case Transform(from, to) =>
        ZIO.succeedBlocking(GutenbergWordCount.run(from.getOrElse(bookTopic), to.getOrElse(wordCountTopic))(runtime))

      case Consume(maybeTopic) =>
        ZIO.succeedBlocking(GutenbergConsumer.run(maybeTopic.getOrElse(wordCountTopic)))
    }
}
