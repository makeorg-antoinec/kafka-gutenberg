package org.make.kafkagutenberg

import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.serialization.{LongDeserializer, StringDeserializer}

import java.time.Duration
import java.util.Properties
import java.util.concurrent.atomic.AtomicBoolean
import scala.jdk.CollectionConverters._

object GutenbergConsumer {
  private val pollDuration = Duration.ofSeconds(1)
  def run(topic: String): Unit = {
    val props = new Properties()
    props.put("group.id", "test")
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.deserializer", classOf[StringDeserializer])
    props.put("value.deserializer", classOf[LongDeserializer])
    props.put("enable.auto.commit", true)
    props.put("auto.commit.interval.ms", 1000)

    val consumer = new KafkaConsumer[String, Long](props)
    val running  = new AtomicBoolean(true)
    try {
      consumer.subscribe(List(topic).asJava)
      println(s"Subscribed to topic ${topic}")
      sys.runtime.addShutdownHook(new Thread(() => {
        running.set(false)
      }))

      while (running.get()) {
        val events = consumer.poll(pollDuration).asScala.map(r => (r.key(), r.value())).toList
        for ((key, value) <- events) {
          println(s"event: key=${key} value=${value}")
        }
      }
    } finally {
      consumer.close()
      println(s"Topic ${topic} closed")
    }
  }
}
