package org.make.kafkagutenberg

import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.kafka.streams.{KafkaStreams, Topology}

import zio.json.JsonCodec
import zio.{ZIO, http}

import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.{Files, Path, StandardOpenOption}
import java.nio.{ByteBuffer, CharBuffer}
import java.util.Properties
import java.util.concurrent.CountDownLatch

object GutenbergWordCount {
  import Serdes._

  private def utfencode(t: CharSequence): Array[Byte] = {
    val encoded = UTF_8.encode(CharBuffer.wrap(t))
    val result  = Array.ofDim[Byte](encoded.remaining())
    encoded.get(result)
    result
  }

  private def utfdecode(t: Array[Byte]): CharSequence = UTF_8.decode(ByteBuffer.wrap(t))

  private implicit def codec2serde[T >: Null](implicit codec: JsonCodec[T]): Serde[T] = Serdes.fromFn[T](
    (t: T) => utfencode(codec.encodeJson(t, None)),
    (xs: Array[Byte]) => codec.decodeJson(utfdecode(xs)).toOption
  )

  def run(bookTopic: String, wordCountTopic: String)(implicit rt: zio.Runtime[Any]): Unit = {
    val streams = new KafkaStreams(buildStreams(bookTopic, wordCountTopic), configureStreams())

    val latch = new CountDownLatch(1)
    sys.runtime.addShutdownHook(new Thread(() => {
      println("Closing streams...")
      streams.close()
      println("Closed streams!")

      latch.countDown()
    }))

    println("Starting streams...")
    streams.start()
    println("Started streams!")

    latch.await()

    println("Closing streams...")
    streams.close()
    println("Closed streams!")
  }

  private def configureStreams(): Properties = {
    val props = new Properties()
    props.put("application.id", "gutenberg-word-count")
    props.put("bootstrap.servers", "localhost:9092")
    props.put("statestore.cache.max.bytes", 0)
    props.put("default.key.serde", Serdes.stringSerde.getClass())
    props.put("default.value.serde", Serdes.stringSerde.getClass())
    props.put("auto.offset.reset", "earliest")
    props.put("num.stream.threads", 8)
    props
  }

  private def buildStreams(bookTopic: String, wordCountTopic: String)(implicit rt: zio.Runtime[Any]): Topology = {
    val builder = new StreamsBuilder()

    val books = builder
      .stream[String, Book](bookTopic)
      .peek((_, book) => println(s"Received book: ${book.title}"))

    val wordCountTable = books
      .mapValues(fetchBookContents(_))
      .flatMapValues(_.toLowerCase().split("\\W+"))
      .groupBy((_, word) => word)
      .count()

    wordCountTable.toStream
      .peek((word, count) => println(s"table: word=$word count=$count"))
      .to(wordCountTopic)

    builder.build()
  }

  private def fetchBookContents(book: Book)(implicit runtime: zio.Runtime[Any]): String =
    zio.Unsafe.unsafe { implicit unsafe =>
      runtime.unsafe.run {
        (for {
          response <- Gutenberg.bookContent(book.id)
          contents <- response.body.asString
          _ <- ZIO.attemptBlockingIO {
            Files.writeString(Path.of("/tmp/gutenberg-last-book.txt"), contents, StandardOpenOption.WRITE, StandardOpenOption.CREATE)
          }
        } yield contents).provide(
          http.Client.customized,
          http.netty.client.NettyClientDriver.live,
          http.DnsResolver.default,
          zio.ZLayer.succeed(http.netty.NettyConfig.default),
          zio.ZLayer.succeed(http.Client.Config.default)
        )
      }.getOrThrow()
    }
}
