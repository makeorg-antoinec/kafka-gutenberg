package org.make.kafkagutenberg

import org.apache.commons.csv.CSVRecord

import zio.json.{DeriveJsonCodec, JsonCodec}

final case class Book(
    id: String,
    `type`: String,
    issued: String,
    title: String,
    language: String,
    authors: String,
    subjects: String,
    locc: String,
    bookshelves: String
)

object Book {
  implicit val codec: JsonCodec[Book] = DeriveJsonCodec.gen

  def fromCSV(record: CSVRecord): Book =
    Book(
      record.get(0),
      record.get(1),
      record.get(2),
      record.get(3),
      record.get(4),
      record.get(5),
      record.get(6),
      record.get(7),
      record.get(8)
    )
}
