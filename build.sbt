ThisBuild / scalaVersion     := "2.13.12"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "org.make"
ThisBuild / organizationName := "Make.org"

lazy val root = (project in file("."))
  .settings(
    name := "kafka-gutenberg",
    libraryDependencies ++= Seq(
      "org.apache.commons" % "commons-csv" % "1.10.0",
      "org.apache.kafka" % "kafka-clients" % "3.6.0",
      "org.apache.kafka" %% "kafka-streams-scala" % "3.6.0",
      "ch.qos.logback" % "logback-classic" % "1.4.11" % Runtime,
      "dev.zio" %% "zio-cli" % "0.5.0",
      "dev.zio" %% "zio-http" % "3.0.0-RC2",
      "dev.zio" %% "zio-json" % "0.6.2",
    ),
    Compile/run/fork := true,
    Compile/run/mainClass := Some("org.make.kafkagutenberg.App"),
    Compile/run/javaOptions += "-Djava.net.preferIPv4Stack=true",
    Universal/javaOptions += "-J-Djava.net.preferIPv4Stack=true",
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision,
    scalacOptions ++= Seq("-Ywarn-unused", "-deprecation")
  )
enablePlugins(JavaAppPackaging)

addCommandAlias("fmt", "scalafmtAll;scalafixAll;compile;")

